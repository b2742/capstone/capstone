const {exchangeRates} = require('../src/util.js');

module.exports = (app) => {

	app.get('/', (req,res) => {
		return res.send({data: {}})
	});


	app.post('/currency', (req,res) => {
		if(!req.body.hasOwnProperty('name')) {
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}

		if(typeof(req.body.name) !== 'string') {
			return res.status(400).send({
				'error' : 'Bad Request : required parameter NAME must be of type string'
			})
		}

		if(req.body.name === "") {
			return res.status(400).send({
				'error' : 'Bad Request : required parameter NAME must not be an empty string'
			})
		}

		if(!req.body.hasOwnProperty('ex')) {
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter EX'
			})
		}

		if(typeof(req.body.ex) !== 'object') {
			return res.status(400).send({
				'error' : 'Bad Request : required parameter EX must be of type object'
			})
		}

		if(Object.entries(req.body.ex).length === 0) {
			return res.status(400).send({
				'error' : 'Bad Request : required parameter EX must not be an empty object'
			})
		}

		if (!req.body.hasOwnProperty('alias')) {
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter ALIAS'
			})
		}

		if(typeof(req.body.alias) !== 'string') {
			return res.status(400).send({
				'error' : 'Bad Request : required parameter ALIAS must be of type string'
			})
		}

		if(req.body.alias.length === 0) {
			return res.status(400).send({
				'error' : 'Bad Request : required parameter ALIAS must not be an empty string'
			})
		}

		if(req.body.alias.length !== 0 && req.body.name !== 0 && Object.entries(req.body.ex).length !== 0) {
			for(const prop in exchangeRates) {
				if(exchangeRates[prop].alias === req.body.alias) {
					return res.status(400).send({'error' : 'Bad Request : required parameter ALIAS already exist'})
				}
			}
		}

		if(req.body.alias.length !== 0 && req.body.name !== 0 && Object.entries(req.body.ex).length !== 0) {
			for(const prop in exchangeRates) {
				if(exchangeRates[prop].alias !== req.body.alias) {
					return res.status(200).send('Success')
				}
			}
		}
		// return res.status(200).send('success')
	})
}