const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
const url = 'http://localhost:5001';

chai.use(http);

describe('API Test Suite', () => {
	it('Test if post /currency is running', () => {
		chai.request(url).post('/currency')
		.end((err,res) => {
			expect(res).to.not.equal(undefined)
		})
	})

	it('Return 400 if name is missing', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        // 'name': 'United States Dollar',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if name is not a string', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        'name': 123,
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			console.log(res.status)
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if name is an empty string', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        'name': '',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			console.log(res.status)
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if ex is missing', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        'name': 'United States Dollar',
	     //  	'ex': {
	     //    	'peso': 50.73,
	     //    	'won': 1187.24,
	     //    	'yen': 108.63,
	     //    	'yuan': 7.03
	    	// }
		})
		.end((err,res)=> {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if ex is not an object', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        'name': 'United States Dollar',
	      	'ex': "iamastring"	    	
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if ex is an empty object', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        'name': 'United States Dollar',
	      	'ex': {}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();	
		})
	})

	it('Return 400 if alias is missing', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			// 'alias' : 'dollars',
	        'name': 'United States Dollar',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if alias is not of type string', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 123,
	        'name': 'United States Dollar',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if alias is an empty string', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : '',
	        'name': 'United States Dollar',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 400 if all fields are complete but has a duplicate alias', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'dollars',
	        'name': 'United States Dollar',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('Return 200 if all fields are complete and has no duplicate alias', (done) => {
		chai.request(url)
		.post('/currency')
		.type('json')
		.send({
			'alias' : 'sample alias',
	        'name': 'sample name',
	      	'ex': {
	        	'peso': 50.73,
	        	'won': 1187.24,
	        	'yen': 108.63,
	        	'yuan': 7.03
	    	}
		})
		.end((err,res) => {
			expect(res.status).to.equal(200)
			done();
		})
	})

})